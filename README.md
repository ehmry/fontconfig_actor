# Fontconfig_actor

A [Syndicate actor](https://syndicate-lang.org) for querying [Fontconfig](https://www.freedesktop.org/software/fontconfig/).

The actor responds to observations of font properties by asserting what it determines to be the most appropriate font.

The assertion format is `<fontconfig {pattern…} {attributes…}>` (see [protocol.prs](./protocol.prs)). The `pattern` field is a dictionary of properties to match fonts against and the `attributes` field is a dictionary of the properties of a font selected by Fontconfig. An application observes a `pattern` and takes the `file` and `index` fields of a corresponding assertion and reads font data from the file-system. In the case that `pattern` does not match any fonts an assertion will still be made, so it is possible that all of the properties in `pattern` will be contradicted in `attributes`.

For a list of possibly supported properties see the [Fontconfig documentation](https://www.freedesktop.org/software/fontconfig/fontconfig-devel/x19.html). When the value of a property is a Preserves symbol (rather than a string in double-quotes) it will be resolved to a Fontconfig constant if possible. Constants are not resolved from integers to symbols in the `attributes` field. For example, `{slant: oblique}` is transformed to `{slant: 110}` from `pattern` to `attributes`.

Example [Syndicate server](https://git.syndicate-lang.org/syndicate-lang/syndicate-rs) configuration:
```
let ?fontspace = dataspace
<fontspace $fontspace>

<daemon fontconfig_actor {
  argv: [ "/usr/local/bin/fontconfig_actor" ]
  protocol: application/syndicate
}>

? <fontspace ?fontspace> [
  <require-service <daemon fontconfig_actor>>
  ? <service-object <daemon fontconfig_actor> ?cap> [
    $cap { dataspace: $fontspace }
    $socketspace ? <fontconfig {size: 8.0, spacing: proportional} ?properties> [
      $log ! <log "-" { line: <fontconfig $properties> }>
    ]
  ]
]
```

# Motivation

This actor is only useful with applications that already speak Syndicate. For those applications that do, getting typeface and font configuration over Syndicate has a few benefits over using Fontconfig directly.
- One less library to compile, link, and load.
- Request caching across different applications and invocations.
- Less system-calls at the application side.
- Dynamic font configuration.
