
import
  preserves, std/tables

type
  FontAssertion* {.preservesRecord: "fontconfig".} = object
    `pattern`*: Properties
    `attributes`*: Properties

  BootArguments* {.preservesDictionary.} = object
    `dataspace`* {.preservesEmbedded.}: Preserve[void]

  Properties* = Table[Symbol, Preserve[void]]
proc `$`*(x: FontAssertion | BootArguments | Properties): string =
  `$`(toPreserve(x))

proc encode*(x: FontAssertion | BootArguments | Properties): seq[byte] =
  encode(toPreserve(x))
