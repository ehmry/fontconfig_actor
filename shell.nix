{ pkgs ? import <nixpkgs> { } }:

pkgs.buildNimPackage {
  name = "dummy";
  nativeBuildInputs = [ pkgs.pkg-config ];
  buildInputs = [ pkgs.fontconfig ];
  lockFile = ./lock.json;
}
