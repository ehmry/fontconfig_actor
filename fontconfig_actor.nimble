# Package

version = "20231130"
author        = "Emery Hemingway"
description   = "Syndicate actor for asserting Fontconfig information"
license       = "Unlicense"
srcDir        = "src"
bin           = @["fontconfig_actor"]


# Dependencies

requires "nim >= 2.0.0", "syndicate >= 20230816"
